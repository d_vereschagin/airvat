import firebase from 'firebase';
import {
    TABLE_GET,
    REQUEST,
    SUCCESS,
    TABLE_PAGE_ADD,
    LASTDOCUMENT_ADD,
    TABLE_SORT_BY,
    TABLE_TOTAL_COUNT,
    TABLE_SET_CURRENT_PAGE,
    FAILURE,
    TABLE_SETUP_FILTER,
} from '../store/action-types';
import action from './action-generic';
import { getSortedElement, getConfigQuery } from '../utils/getters'; 
import normalizeArray from '../utils/transform-array';
import moment from 'moment';
import { Dispatch } from 'redux';
import { AppState } from '../reducers/global-state';

let res:any;
let fullArray:any;

firebase.initializeApp({
    apiKey: "AIzaSyAAyleaXrTxgaE_kgLaOcUnYXMSjDkDElY",
    authDomain: "airvat-9d78c.firebaseapp.com",
    databaseURL: "https://airvat-9d78c.firebaseio.com/",
    projectId: "airvat-9d78c",
});

const db = firebase.firestore();

export const getData = () => async (dispatch:Dispatch) => {
    try {

        dispatch(action(TABLE_GET[REQUEST]));

        const limitArray:any = await db.collection('users').orderBy('firstName').limit(25).get();
        res = limitArray.docs[limitArray.docs.length - 1];

        const data = normalizeArray(limitArray);
        
        dispatch(action(TABLE_GET[SUCCESS], data));

        dispatch(action(LASTDOCUMENT_ADD, limitArray.docs[limitArray.docs.length - 1]));         

    } catch(error) {
        dispatch(action(TABLE_GET[FAILURE]));
        console.log(error);        
    }
};

export const writeInputInfo = (data:Date) => (dispatch:Dispatch) => {
    dispatch(action(TABLE_SETUP_FILTER, data));
};

export const sortData = (value:any) => async (dispatch:any, getState:any) => {
    try {

        const sortedArray = getSortedElement(getState());
        const configQuery= getConfigQuery(getState());

        let query = db.collection('users').orderBy(configQuery.sortColumns, configQuery.sortBy);

        for (let i = 0; i < sortedArray.length; i++) {
            if(sortedArray[i].value !== '') {
                if(sortedArray[i].name == 'email') {
                    query = query.where(sortedArray[i].name, '>=', sortedArray[i].name)
                } else if(sortedArray[i].name == 'lastActive' && moment(sortedArray[i].value).isValid()) {
                    query = query.where(sortedArray[i].name, '>=', +sortedArray[i].value)
                } else {
                    query = query.where(sortedArray[i].name, '>=', sortedArray[i].name.toUpperCase());
                }
            }
        }

        let result = await query.limit(25).get();
        
        const data = normalizeArray(result);

        dispatch(action(TABLE_GET[SUCCESS], data));
        dispatch(action(TABLE_SORT_BY, value));
        res = result.docs[result.docs.length - 1];
    } catch(error) {
        console.log(error);
    }
  
};

export const filterData = (value:string) => async (dispatch:any, getState:any) => {
    try {

        const sortedArray = getSortedElement(getState());
        const configQuery= getConfigQuery(getState());

        let query = db.collection('users').orderBy(value, configQuery.sortBy);

        for (let i = 0; i < sortedArray.length; i++) {
            if(sortedArray[i].value !== '') {
                if(sortedArray[i].name == 'email') {
                    query = query.where(sortedArray[i].name, '>=', sortedArray[i].value)
                } else if(sortedArray[i].name == 'lastActive' && moment(sortedArray[i].value).isValid()) {
                    query = query.where(sortedArray[i].name, '>=', +sortedArray[i].value)
                } else {
                    query = query.where(sortedArray[i].name, '>=', sortedArray[i].value.toUpperCase());
                }
            }
        }

        let result = await query.limit(25).get();

        
        const data = normalizeArray(result);

        dispatch(action(TABLE_GET[SUCCESS], data));

        res = result.docs[result.docs.length - 1];
    } catch(error) {
        console.log(error);
    }
  
};

export const loadNextPage = () => async (dispatch:Dispatch, getState:() => AppState) => {
    try {
        const result = await db.collection('users').orderBy('firstName').startAfter(res).limit(25).get();

        dispatch(action(LASTDOCUMENT_ADD, result.docs[result.docs.length - 1]));

        res = result.docs[result.docs.length - 1];

        const data = normalizeArray(result);

        dispatch(action(TABLE_PAGE_ADD, data));

    } catch(error) {
        console.log(error);
    }
};

export const loadPrevPage = () => async (dispatch:Dispatch, getState:() => AppState) => {
    try {
        const result = await db.collection('users').orderBy('firstName').endBefore(res).limit(25).get();
        res = result.docs[result.docs.length - 1];
        const data = normalizeArray(result);

        dispatch(action(TABLE_PAGE_ADD, data));

    } catch(error) {
        console.log(error);
    };
}
