import React, { Component } from 'react';
import TablePage from '../src/containers/TablePage';
import { Provider } from 'react-redux';
import './App.scss';
import configureStore from './store/configure-store';

const store = configureStore((window as any).__INITIAL_STATE__);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <TablePage />
      </Provider>
    );
  }
}

export default App;
