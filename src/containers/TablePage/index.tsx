import React, { Component, Fragment, SFC } from 'react';
import { Header, Table } from '../../components';

export const TablePage:SFC = () => (
    <Fragment>
    <Header />
    <Table />
  </Fragment>
);
export default TablePage;