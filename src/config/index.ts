import firebase from 'firebase';

export const initializeApp = firebase.initializeApp({
    apiKey: "AIzaSyAAyleaXrTxgaE_kgLaOcUnYXMSjDkDElY",
    authDomain: "airvat-9d78c.firebaseapp.com",
    databaseURL: "https://airvat-9d78c.firebaseio.com/",
    projectId: "airvat-9d78c",
});
