export default (data:any) => {
    let transformedData:any;

   data.forEach((item:any) => {
        transformedData = {
            ...transformedData,
            [item.id]: item.data()
        };   
    });

    const result = Object.keys(transformedData).map(id => ({
        ...transformedData[id],
        account: {
            ...transformedData[id].account,
            id
        }
    }));

    return result;
};