import { AppState } from '../reducers/global-state';

export const getCurrentPage = (state:AppState) => state.tableData.configQuery.currentPage;
export const getSortedElement = (state:AppState) => state.tableData.sort;
export const getConfigQuery = (state:AppState) => state.tableData.configQuery;