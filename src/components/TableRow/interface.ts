import { TableState } from '../../reducers/Table/types';

export default interface Props {
    row: [TableState]
};