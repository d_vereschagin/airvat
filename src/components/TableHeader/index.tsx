import React, { Component, ReactEventHandler, FormEvent } from "react";
import styles from './styles.scss';
import DatePicker from 'react-date-picker';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import _ from 'lodash';
import { connect } from 'react-redux'; 
import { bindActionCreators } from 'redux';
import { sortData, writeInputInfo, filterData } from '../../actions';
import upArrow from '../../assets/img/up-arrow.svg';
import downArrow from '../../assets/img/down-arrow.svg';
import { AppState } from '../../reducers/global-state';

interface Props {
  sort: (data:object) => void,
  inputInfoWrite: (data:object) => void,
  dataFilter: (value:string) => void,
  config: {
    sortBy: "asc" | "desc" | undefined,
    sortColumns: string,
  }
}

interface State {
  selectOption: any,
  currentEnterValue: string | Date,
  textContent: string,
  selectedTime: Date
}

class TableHeader extends Component<Props,State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      selectOption: 'asc',
      currentEnterValue: '',
      textContent: 'firstName',
      selectedTime: new Date()
    };
  };

  handleClickSort = (event:any):void => {

    const id = event.target.parentNode.parentNode.id;

    this.setState((prevState) => {
      return {
        selectOption: prevState.selectOption == 'asc' ? 'desc' : 'asc',
        textContent: id
      }
    }, () => {

      const { sort }  = this.props;
      const { selectOption, textContent } = this.state;
  
      sort({ sortBy: selectOption, sortColumns: `${textContent}`});
    });
  };

  handleChange = (selectedOption:any):void => {

    this.setState({ selectOption: selectedOption });
  };

  handleSortInput = (name:string, value:string) => {
    const { inputInfoWrite, dataFilter } = this.props;

    dataFilter(name);

    inputInfoWrite({ name, value });
  };


  handleFilterInput = (name:string):ReactEventHandler<HTMLInputElement> => (e:FormEvent<HTMLInputElement>)  => {
    const { inputInfoWrite } = this.props;
    const value = e.currentTarget.value;

    this.setState({ currentEnterValue: e.currentTarget.value }, () => {
      _.debounce(() => this.handleSortInput(name, value), 300)();
    });

    inputInfoWrite(
      { 
        name,
        value: e.currentTarget.value,
      }
    );
  };
  
  handleDateChange = (value:any) => {
    const columnName = "lastActive";
    const { inputInfoWrite, sort, config } = this.props;

    this.setState({ currentEnterValue: value });

    inputInfoWrite({
      name: columnName,
      value,
    });
    
    sort({ sortBy: config.sortBy, sortColumns: columnName });
  }

  renderImage = () => {
    const { sortBy, sortColumns } = this.props.config;

    if (sortBy == 'asc' && sortColumns == 'firstName') {
      return upArrow;
    };
  
    return downArrow;
  }

    render() {
      const { config } = this.props;

      return (
        <thead>
          <tr>  
            <th
              className={styles.tableHeaderCell}
            >
                {'User ID'}
            </th>
            <th
              id="firstName"
              className={styles.tableHeaderCell}
              >
            <div className={styles.dropDownContainer}>
              <p className={styles.text}>{'firstName'}</p>
              <img 
                onClick={(event) => this.handleClickSort(event)}
                className={styles.image} 
                src={config.sortBy == 'asc' && config.sortColumns == 'firstName' ? downArrow : upArrow} 
                width="25" 
                height="25" 
                />
            </div>
            </th>
            <th
              id="surname"
              className={styles.tableHeaderCell}
              >
             <div className={styles.dropDownContainer}>
                <p className={styles.text}>{'surname'}</p>
                
                <img
                  className={styles.image} 
                  onClick={(event) => this.handleClickSort(event)}
                  src={config.sortBy == 'asc' && config.sortColumns == 'surname' ? downArrow : upArrow }
                  width="25"
                  height="25"
                />
              </div>
            </th>
            <th
              id="email"
              className={styles.tableHeaderCell}
            >
              <div className={styles.dropDownContainer}>
                <p className={styles.text}>{'email'}</p>
                <img
                  onClick={(event) => this.handleClickSort(event)}
                  className={styles.image} 
                  src={config.sortBy == 'asc' && config.sortColumns == 'email' ? downArrow : upArrow}
                  width="25" 
                  height="25"
                />
              </div>
                
            </th>
            <th
              id="account.dob"
              className={styles.tableHeaderCell}>
              <div className={styles.dropDownContainer}>
                <p className={styles.text}>{'account.dob'}</p>
                <img
                  onClick={(event) => this.handleClickSort(event)}
                  className={styles.image}
                  src={config.sortBy == 'asc' && config.sortColumns == 'account.dob' ? downArrow : upArrow}
                  width="25"
                  height="25" />
              </div>
            </th>

            <th
              id="account.residenceCountry"
              className={styles.tableHeaderCell}
            >
              <div className={styles.dropDownContainer}>
                <p className={styles.text}>{'residence Country'}</p>
                <img
                  onClick={(event) => this.handleClickSort(event)} 
                  className={styles.image} src={config.sortBy == 'asc' && config.sortColumns == 'account.residenceCountry' ? downArrow : upArrow}
                  width="25" 
                  height="25"
                 />
              </div>
            </th>
            <th
              id="account.residenceCity"
              className={styles.tableHeaderCell}
            >
              <div className={styles.dropDownContainer}>
                <p className={styles.text}>{'residence City'}</p>
                <img
                  onClick={(event) => this.handleClickSort(event)} 
                  className={styles.image} src={config.sortBy == 'asc' && config.sortColumns == 'account.residenceCity' ? downArrow : upArrow}
                  width="25" height="25"
                />
              </div>
            </th>
            <th
              id="lastActive"
              className={styles.tableHeaderCell}
            >
              <div className={styles.dropDownContainer}>
                <p className={styles.text}>{'lastActive'}</p>
                <img
                  onClick={(event) => this.handleClickSort(event)}
                  className={styles.image} src={config.sortBy == 'asc' && config.sortColumns == 'lastActive' ? downArrow : upArrow}
                  width="25"
                  height="25" 
                />
              </div>
            </th>
        </tr>

        <tr>
            <th
              className={styles.tableHeaderCell}
            >
            </th>
            <th
              className={styles.tableHeaderCell}
            >
                <input id="firstName" type="text" onChange={this.handleFilterInput('firstName')} />
            </th>
            <th
              className={styles.tableHeaderCell}
            >
                <input id="surname" type="text" onChange={this.handleFilterInput('surname')} />
            </th>
            <th
              className={styles.tableHeaderCell}
            >
                <input id="email" type="text" onChange={this.handleFilterInput('email')} />
            </th>
            <th
              className={styles.tableHeaderCell}
            >
              <DatePicker
                value={new Date()}
                onChange={(event) => this.handleDateChange(event)}
              />

            </th>
            <th
              className={styles.tableHeaderCell}
            >
              <input id="account.residenceCountry" type="text" onChange={this.handleFilterInput('account.residenceCountry')}/>
            </th>

            <th
              className={styles.tableHeaderCell}
            >
              <input id="account.residenceCity" type="text" onChange={this.handleFilterInput('account.residenceCity')} />
            </th>
            <th
              className={styles.tableHeaderCell}
            >
            <DatePicker
                value={new Date()}
                onChange={(event) => this.handleDateChange(event)}
            />
            </th>
        </tr>  
    </thead>
    );
  }
};

const mapStateToProps = ({ tableData }:AppState) => ({ config: tableData.configQuery });

const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  sort: (data) => dispatch(() => sortData(data)),
  inputInfoWrite: (data) => dispatch(() => writeInputInfo(data)),
  dataFilter: (value:any) => dispatch(() => filterData(value))
}, dispatch);
export default connect(mapStateToProps, mapDispatchToProps)(TableHeader);