import React, { StatelessComponent } from 'react';
import styles from './styles.scss';

export const Header:StatelessComponent<{}> = () => (
    <div className={styles.header}>
        <h1 className={styles.headerTitle}>AirVat</h1>
    </div>  
    );
export default Header;