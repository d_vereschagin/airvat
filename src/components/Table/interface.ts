import { TableState, TableData } from '../../reducers/Table/types';

export default interface Props {
    tableData: TableData,
    getData: () => void,
    nextPageLoad: () => void,
    prevPageLoad: () => void,
}