export const table: string;
export const table__wrap: string;
export const tableWrap: string;
export const table__button: string;
export const tableButton: string;
export const table__container: string;
export const tableContainer: string;
export const table__bord: string;
export const tableBord: string;
export const table__header__cell: string;
export const tableHeaderCell: string;
export const table__body__cell: string;
export const tableBodyCell: string;
export const tableBodyEmpty: string;
export const container: string;
export const activeLink: string;
export const prevActiveLink: string;
export const nextActiveLink: string;
export const buttonContainer: string;
export const button: string;
