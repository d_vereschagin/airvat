import React, { Component } from 'react';
import { connect } from 'react-redux';
import { dataColumns } from '../../utils/mock-data';
import { AppState } from '../../reducers/global-state';
import  TableHeader from '../TableHeader';
import moment from 'moment';
// import Pagination from 'react-paginate';
import Props from './interface';
import styles from './styles.scss';
import { bindActionCreators } from 'redux';
import { getData, loadNextPage, loadPrevPage } from '../../actions';

interface State {
    currentPage: number,
    isFetching: boolean
}

class Table extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      currentPage: 0,
      isFetching: false
    };
  };

  componentDidMount() {
    const { getData }= this.props;

    getData();
  };

  handlePrevPage = (e:any) => {
    const { prevPageLoad } = this.props;
    prevPageLoad();
    // console.log();
  };

  handleNextPage = (e:any) => {
    this.setState({ currentPage: e.selected + 1 }, () => {
      const { nextPageLoad  } = this.props;
      
      nextPageLoad();
    });

  };

  render() {
  const { tableData: { data, isFetching } } = this.props;

    return (
      <div className={styles.tableContainer}>
        <div className={styles.tableWrap}>
          <div className={styles.tableBord}>
            <table className={styles.table}>
              <TableHeader />
              <tbody>
                {isFetching ? (
                <tr>
                    <td className={styles.tableBodyEmpty} colSpan={dataColumns.length}>
                      {'Loading...'}
                    </td>
                </tr>
                ) : data.map(row => (
                  <tr key={row.account.id}>
                  <td className={styles.tableBodyCell}>
                    {row.account.id}
                  </td>
                  <td className={styles.tableBodyCell}>
                    {row.account.firstName}
                  </td>
                  <td className={styles.tableBodyCell}>
                    {row.account.surname}
                  </td>
                  <td className={styles.tableBodyCell}>
                    {row.email}
                  </td>
                  <td className={styles.tableBodyCell}>
                    {moment(row.account.dob).format('DD/MM/YYYY')}
                  </td>
                  <td className={styles.tableBodyCell}>
                    {row.account.residenceCountry}
                  </td>
                  <td className={styles.tableBodyCell}>
                    {row.account.residenceCity}
                  </td>
                  <td
                    data-time={row.lastActive}
                    className={styles.tableBodyCell}>
                    {moment(row.lastActive).format('DD/MM/YYYY')}
                  </td>
                </tr>
                ))
              }
              </tbody>
            </table>
          </div>
          <div className={styles.buttonContainer}>
            <button className={styles.button} onClick={this.handlePrevPage}>Back</button>
            <button className={styles.button} onClick={this.handleNextPage}>Next</button>
          </div>
        </div>
      </div>
    );
  };
};

const mapStateToProps = ({ tableData }: AppState) => ({ tableData });
const mapDispatchToProps = (dispatch:any) => bindActionCreators({
  getData: () => dispatch(getData),
  nextPageLoad: () => dispatch(() => loadNextPage()),
  prevPageLoad: () => dispatch(() => loadPrevPage()),
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Table);