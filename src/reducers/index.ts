import { combineReducers, Reducer } from 'redux';
import { AppState } from './global-state';
import tableData from './Table/get-table';

const reducer:Reducer<AppState> = combineReducers<AppState>({
    tableData,
});

export default reducer;