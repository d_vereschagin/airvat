
export interface TableState {
    account: {
        address1 : string
        address2 : string,
        defaultRefundMethodId : string, 
        displayName : string,
        dob : string,
        id: string,
        firstName : string,
        passportNo? : string,
        residenceCity : string,
        residenceCountry : string,
        surname: string
    },
        email: string, 
        lastActive: number,
        meta: {
        creationTime : number
    },
}

export interface SortElement {
    name: string,
    value: string
}

export interface TableData {
    data: TableState[],
    isFetching: boolean,
    count: object,
    configQuery: {
        sortBy: "asc"|"desc"| undefined,
        sortColumns: string,
        currentPage: number,
    },
    sort: SortElement[]
}