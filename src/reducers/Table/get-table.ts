import { Reducer } from 'redux';
import { TableData } from './types';
import {
  TABLE_GET,
  TABLE_RESET_SORT,
  REQUEST,
  SUCCESS,
  FAILURE,
  TABLE_SORT_BY,
  TABLE_PAGE_ADD,
  TABLE_TOTAL_COUNT,
  TABLE_SET_CURRENT_PAGE,
  TABLE_SETUP_FILTER,
 } from '../../store/action-types';

export const initialState: TableData = {
    data: [],
    isFetching: false,
    count: {},
    configQuery: {
        sortBy: "asc",
        sortColumns: 'firstName',
        currentPage: 1,
    },
    sort: [],
};

let data:[] = [];

const reducer:Reducer<TableData> = (state: TableData=initialState, action) => {
    switch(action.type) {
        case TABLE_GET[REQUEST]:
            return {
               ...state,
               isFetching: true 
            }
        case TABLE_GET[SUCCESS]:
            return {
                ...state,
                data: action.payload,
                isFetching: false,
                configQuery: { ...state.configQuery }
            }

        case TABLE_GET[FAILURE]:
            return {
                ...state,
                isFetching: false,
            }
        case TABLE_TOTAL_COUNT:
            return {
                ...state,
                count: action.payload
            }
        case TABLE_RESET_SORT: {
            return {
                ...state,
                data,
            }
        }
        case TABLE_PAGE_ADD: {
            return {
                ...state,
                data: action.payload,
                configQuery: {
                    ...state.configQuery,
                }
            }
        }
        case TABLE_SETUP_FILTER: {
            return {
                ...state,
                sort: [
                    ...state.sort.filter(item => item.name !== action.payload.name),
                    action.payload,
                ]
            }
        }
        case TABLE_SET_CURRENT_PAGE: {
            return {
                ...state,
                configQuery: {
                    ...state.configQuery,
                    currentPage: action.payload
                }
            }
        }

        case TABLE_SORT_BY: {
            return {
                ...state,
                configQuery: {
                    sortBy: action.payload.sortBy,
                    sortColumns: action.payload.sortColumns,
                    currentPage: 1
                }
            }
        }
        default: return state
    }
};

export default reducer;