import { TableData } from './Table/types';

export interface AppState {
    tableData: TableData,
}