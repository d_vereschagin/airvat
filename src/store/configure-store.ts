import { applyMiddleware, createStore, Store } from 'redux';
import { compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';
import { AppState } from '../reducers/global-state';

export default (initialState: AppState):Store<AppState> => {
    const composeEnhancers = (window as any)['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose || compose;
    const store = createStore(
        reducer,
        initialState,
        composeEnhancers(applyMiddleware(thunk))
    );;
    return store;

}