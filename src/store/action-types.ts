
export const REQUEST = 'REQUEST';
export const SUCCESS = 'SUCCESS';
export const FAILURE = 'FAILURE';

const createTypes = (base: string) => [REQUEST, SUCCESS, FAILURE].reduce((acc, type: any) => {
  acc[type] = `${base}_${type}`;
  return acc;
}, {});

// Table
export const TABLE_GET = createTypes('TABLE/GET');
export const TABLE_RESET_SORT = 'TABLE_RESET_SORT';
export const TABLE_SORT_BY = 'TABLE/SORT_BY';
export const TABLE_PAGE_ADD = 'TABLE/PAGE_ADD';
export const TABLE_SET_CURRENT_PAGE = 'TABLE_SET_CURRENT_PAGE';
export const TABLE_TOTAL_COUNT = 'TABLE/TOTAL_COUNT';
export const TABLE_SETUP_FILTER = 'TABLE_SETUP_FILTER';

// Last document
export const LASTDOCUMENT_ADD = 'LASTDOCUMENT/ADD';
